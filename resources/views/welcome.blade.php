<!doctype html>
<html lang="en">

<head>
    <title>NSystem Solutions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="copyright" content="NSystem solutions was developed by John Paul."/>
    <meta name="description" content="Get in touch and request a quote for the development of your system, website or e-commerce. We are ready to serve you in the best possible way. On-time deliveries, with fair prices and excellent service.">
    <meta name="keywords" content="Developer, developing, web design, web developer, ecommerce, systems, graphic design, website builder, web designer, ecommerce website, web design company, webdesign, responsive web design"/>
    <meta name="robots" content="index"/>
    <meta name="DC.title" content="Systems and website development "/>
    <link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet">

    <link href="data:image/x-icon;base64,YourBase64StringHere" rel="icon" type="images/favicon.png" />

    <link rel="stylesheet" href="fonts/icomoon/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css/aos.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">


<div class="site-wrap" id="home-section">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>



    <header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
            <div class="row align-items-center position-relative">

                <div class="col-3 ">
                    <div class="site-logo">
                        <a href="{{ url('/') }}" class="font-weight-bold">NSS.</a>
                    </div>
                </div>


            </div>
        </div>

    </header>

    <div class="ftco-blocks-cover-1">
        <div class="site-section-cover overlay" style="background-image: url('images/hero_1.jpg')">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-7 text-center">
                        <h1 class="mb-4 text-white">NSystem Solutions</h1>
                        <p class="mb-4">Start developing your ideas now, we are here to help you get your project off the ground. Contact us now, we guarantee quality service and great delivery time :)</p>
                        <p><a href="#contact" class="btn btn-primary btn-outline-white py-3 px-5">Contact Us</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <h2 class="h4 mb-4">About Us</h2>
                    <p>We work with the development of websites, systems and ecommerce. We value punctual delivery, and the fair price. As we develop you can see the progress with your website, we send our results to the clients until the project is completed.</p>
                    <p></p>
                    <p><a href="#portfolio" class="btn btn-primary text-white px-5">Our works</a></p>
                </div>
                <div class="col-md-4">
                    <img src="images/about_1.jpg" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-4">

                    <h2 class="h4 mb-4">Our expertise and skills</h2>

                    <div class="progress-wrap mb-4">
                        <div class="d-flex">
                            <span>PHP</span>
                            <span class="ml-auto">100%</span>
                        </div>
                        <div class="progress rounded-0" style="height: 7px;">
                            <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <div class="progress-wrap mb-4">
                        <div class="d-flex">
                            <span>Frameworks</span>
                            <span class="ml-auto">90%</span>
                        </div>
                        <div class="progress rounded-0" style="height: 7px;">
                            <div class="progress-bar" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <div class="progress-wrap mb-4">
                        <div class="d-flex">
                            <span>Bootstrap</span>
                            <span class="ml-auto">93%</span>
                        </div>
                        <div class="progress rounded-0" style="height: 7px;">
                            <div class="progress-bar" role="progressbar" style="width: 93%;" aria-valuenow="93" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <div class="progress-wrap mb-4">
                        <div class="d-flex">
                            <span>jQuery</span>
                            <span class="ml-auto">90%</span>
                        </div>
                        <div class="progress rounded-0" style="height: 7px;">
                            <div class="progress-bar" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light" >
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-7 mx-auto text-center">
                    <h2 class="heading-29190">Our Services</h2>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6 col-lg-4">
                    <div class="service-29128 text-center">
              <span class="d-block wrap-icon">
                <span class="icon-desktop_mac"></span>
              </span>
                        <h3>Web Design</h3>
                        <p>For the development of simple websites, we have a base price of £15 to £20 per hour.</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="service-29128 text-center">
              <span class="d-block wrap-icon">
                <span class="icon-desktop_mac"></span>
              </span>
                        <h3>Web Systems</h3>
                        <p>A well-built system within a company generates facilities and agility, also reducing errors that may happen. The price varies according to the demand.</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="service-29128 text-center">
              <span class="d-block wrap-icon">
                <span class="icon-desktop_mac"></span>
              </span>
                        <h3>Ecommerce</h3>
                        <p>Have a well-built online store with a fluid and beautiful layout for your customers. And sell a lot more.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section" id="portfolio">
        <div class="container">

            <div class="row mb-5">
                <div class="col-md-7 mx-auto text-center">
                    <h2 class="heading-29190">Our Works</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="item web">
                        <a href="images/g4.png" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/4.jpg">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="item web">
                        <a href="images/g3.png" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/3.jpg">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="item web">
                        <a href="images/g1.png" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/1.jpg">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="item web">
                        <a href="images/g2.png" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/2.jpg">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="item web">
                        <a href="images/g5.png" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/5.jpg">
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="item web">
                        <a href="images/g6.png" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/6.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section" id="contact">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-7 mx-auto text-center">
                    <h2 class="heading-29190">Contact us</h2>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-8 mb-5" >
                    <form action="{{ route('sendEmail') }}" method="get">
                        <div class="form-group row">
                            <div class="col-md-6 mb-4 mb-lg-0">
                                <input type="text" class="form-control" name="first_name" placeholder="First name">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="last_name" placeholder="Last name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="email" placeholder="Email address">
                            </div>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="phonenumber"  id="phonenumber" placeholder="Phone number">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <textarea name="mensagem" id="" class="form-control" placeholder="Write your message." cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 mr-auto">
                                <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Send Message">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 ml-auto">
                    <div class="bg-white p-3 p-md-5">
                        <h3 class="text-black mb-4">Contact Info</h3>
                        <ul class="list-unstyled footer-link">
                            <li class="d-block mb-3">
                            <li class="d-block mb-3"><span class="d-block text-black">Phone:</span><span>+44 07375 926730</span></li>
                            <li class="d-block mb-3"><span class="d-block text-black">Email:</span><span>contact@nsystemsolutions.co.uk</span></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--
        <div class="site-section bg-light">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                <div class="fact-39281 d-flex align-items-center">
                  <div class="wrap-icon mr-3">
                    <span class="icon-smile-o"></span>
                  </div>
                  <div class="text">
                    <span class="d-block">83</span>
                    <span>Happy Clients</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                <div class="fact-39281 d-flex align-items-center">
                  <div class="wrap-icon mr-3">
                    <span class="icon-coffee"></span>
                  </div>
                  <div class="text">
                    <span class="d-block">3892</span>
                    <span>Cup of Coffee</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                <div class="fact-39281 d-flex align-items-center">
                  <div class="wrap-icon mr-3">
                    <span class="icon-code"></span>
                  </div>
                  <div class="text">
                    <span class="d-block">3,923,892</span>
                    <span>Line of Codes</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                <div class="fact-39281 d-flex align-items-center">
                  <div class="wrap-icon mr-3">
                    <span class="icon-desktop_mac"></span>
                  </div>
                  <div class="text">
                    <span class="d-block">3892</span>
                    <span>Project Finish</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="site-section">
          <div class="container">
            <div class="row mb-5">
              <div class="col-md-7 mx-auto text-center">
                <h2 class="heading-29190">See Our Studio</h2>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="col-md-8">

                <a href="https://vimeo.com/191947042" data-fancybox  class="btn-video_38929">
                  <span><span class="icon-play"></span></span>
                  <img src="images/img_1.jpg" alt="Image" class="img-fluid">
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="site-section bg-light">
          <div class="container">

            <div class="row mb-5">
              <div class="col-md-7 mx-auto text-center">
                <h2 class="heading-29190">Testimonials</h2>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4 col-md-6">

                <div>
                  <div class="person-pic-39219 mb-4">
                    <img src="images/person_1.jpg" alt="Image" class="img-fluid">
                  </div>

                  <blockquote class="quote_39823">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas excepturi accusantium non aut perspiciatis nisi magni libero, molestias.</p>
                  </blockquote>
                  <p>&mdash; Chris Smith</p>
                </div>
              </div>
              <div class="col-lg-4 col-md-6">


                <div>
                  <div class="person-pic-39219 mb-4">
                    <img src="images/person_2.jpg" alt="Image" class="img-fluid">
                  </div>
                  <blockquote class="quote_39823">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas excepturi accusantium non aut perspiciatis nisi magni libero, molestias.</p>
                  </blockquote>
                  <p>&mdash; Chris Smith</p>
                </div>

              </div>
              <div class="col-lg-4 col-md-6">

                <div>
                  <div class="person-pic-39219 mb-4">
                    <img src="images/person_3.jpg" alt="Image" class="img-fluid">
                  </div>
                  <blockquote class="quote_39823">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas excepturi accusantium non aut perspiciatis nisi magni libero, molestias.</p>
                  </blockquote>
                  <p>&mdash; Chris Smith</p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="site-section bg-white">
          <div class="container">
            <div class="row mb-5">
              <div class="col-md-7 mx-auto text-center">
                <h2 class="heading-29190">Blog</h2>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4 col-md-6 mb-4">
                <div class="post-entry-1 h-100">
                  <a href="single.html">
                    <img src="images/img_1.jpg" alt="Image"
                     class="img-fluid">
                  </a>
                  <div class="post-entry-1-contents">

                    <h2><a href="single.html">Lorem ipsum dolor sit amet</a></h2>
                    <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 mb-4">
                <div class="post-entry-1 h-100">
                  <a href="single.html">
                    <img src="images/img_2.jpg" alt="Image"
                     class="img-fluid">
                  </a>
                  <div class="post-entry-1-contents">

                    <h2><a href="single.html">Lorem ipsum dolor sit amet</a></h2>
                    <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 col-md-6 mb-4">
                <div class="post-entry-1 h-100">
                  <a href="single.html">
                    <img src="images/img_3.jpg" alt="Image"
                     class="img-fluid">
                  </a>
                  <div class="post-entry-1-contents">

                    <h2><a href="single.html">Lorem ipsum dolor sit amet</a></h2>
                    <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        -->

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <h2 class="footer-heading mb-3">About Me</h2>
                    <p>My name is Joao Paulo, but you can call me John. I have a software development company in Brazil, and I intend to expand my business abroad.</p>
                    <hr>
                    <div class="item web">
                        <a href="images/perfil.jpg" class="item-wrap" data-fancybox="gal">
                            <span class="icon-add"></span>
                            <img class="img-fluid" src="images/perfil.jpg">
                        </a>
                    </div>
                </div>

            </div>
            <div class="row pt-5 mt-5 text-center">
                <div class="col-md-6">
                    <div class="border-top pt-5">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="border-top pt-5">
                        <script type="text/javascript"> //<![CDATA[
                            var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
                            document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                            //]]></script>
                        <script language="JavaScript" type="text/javascript">
                            TrustLogo("https://www.positivessl.com/images/seals/positivessl_trust_seal_lg_222x54.png", "POSDV", "none");
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/jquery-migrate-3.0.0.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/aos.js"></script>

<script src="js/main.js"></script>
<script src="{{ url('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>

<script type="text/javascript">
    @if (session()->has('status') && session()->get('status') == 200)
    swal("Sucesso!", "{{ session()->get('msg') }}", "success");
    @endif
    @if (session()->has('status') && session()->get('status') == 400)
    swal("Erro!", "{{ session()->get('msg') }}", "error");
    @endif
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134830670-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-134830670-1');
</script>


</body>

</html>
