<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class MailController extends Controller
{
    public function basic_email(Request $request) {
        $data = array(
            'name' => $request->first_name . ' ' . $request->last_name,
            'phonenumber' => $request->phonenumber,
            'email' => $request->email,
            'mensagem' => $request->mensagem);

        try {
            Mail::send(['text'=>'mail'], $data, function($message) {
                $message->to('contact@nsystemsolutions.co.uk', 'Contato')->subject
                ('Formulario de contato');
                $message->from('contact@nsystemsolutions.co.uk','Joao');
            });
        }catch (Exception $e){
            return response()->json([
                'status' => 400,
                'msg' => $e->getMessage()
            ]);
        }


        return redirect('/')->with('status', 200)->with('msg', 'Message sent successfully.');
    }

    public function html_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel HTML Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "HTML Email Sent. Check your inbox.";
    }

    public function attachment_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
}
